package com.daioware.security;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class Md5Encoder implements PasswordEncoder{
	private Md5PasswordEncoder md5Encoder=new Md5PasswordEncoder();
	
	public String encode(String string) {
		return md5Encoder.encodePassword(string, null);
	}
	public boolean matches(String str,String encode) {
		return md5Encoder.encodePassword(str,null).equals(encode);
	}
	
}
