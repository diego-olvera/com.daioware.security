package com.daioware.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class MyPasswordEncoder implements PasswordEncoder{
	private BCryptPasswordEncoder bcryptEncoder=new BCryptPasswordEncoder();

	@Override
	public String encode(String str) {
		return bcryptEncoder.encode(str);
	}

	@Override
	public boolean matches(String str,String encoded) {
		return bcryptEncoder.matches(str, encoded);
	}

}
