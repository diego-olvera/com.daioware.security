package com.daioware.security;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AesEncrypter implements Encrypter{
	
	public static final int KEY_SIZE=16;
	
	public byte[] encrypt(String key,byte[] bytes) {
		try {
			key=String.format("%"+KEY_SIZE+"s",key);
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
	        Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
	        return cipher.doFinal(bytes);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	public byte[] decrypt(String key,byte[] bytes) {
		try {
			key=String.format("%"+KEY_SIZE+"s",key);
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
	        Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
	        return cipher.doFinal(bytes);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	public static void main(String[] args) {
		AesEncrypter encrypter=new AesEncrypter();
		String text="Hey, leonard, hello, darkness";
		String key="hola";
		byte[] bytes;		
		System.out.println(key);
		System.out.println(key.length());
		System.out.println(new String(bytes=encrypter.encrypt(key,text.getBytes())));
		System.out.println(new String(encrypter.decrypt(key,bytes)));
	}
}
