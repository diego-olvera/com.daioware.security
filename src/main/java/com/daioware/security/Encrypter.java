package com.daioware.security;

public interface Encrypter {
	public abstract byte[] encrypt(String key,byte[] bytes);
	public abstract byte[] decrypt(String key,byte[] bytes);
	
	public default byte[] encrypt(String key,char[] chars){
		return encrypt(key,new String(chars));
	}
	public default byte[] encrypt(String key,String s){
		return encrypt(key,s.getBytes());
	}
}