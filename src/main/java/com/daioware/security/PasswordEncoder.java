package com.daioware.security;

public interface PasswordEncoder {

	public String encode(String str);
	
	public boolean matches(String str,String encoded);
	
	public default boolean matches(CharSequence str,String encoded) {
		return matches(str.toString(),encoded);
	}
}
