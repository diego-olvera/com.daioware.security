package com.daioware.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BsCrypterEncoder {
	private static BCryptPasswordEncoder bcryptEncoder=new BCryptPasswordEncoder();

	public static String encode(String string) {
		return bcryptEncoder.encode(string);
	}
	public static boolean matches(String stringToMatch,String encode) {
		return bcryptEncoder.matches(stringToMatch, encode);
	}
}
